![Semapi Logo](http://www.semapi.com.br/data/imgs/logo_header.png)
![Usemol Logo](https://www.usemol.com.br/mol/imagens/logo-usemol.jpg)

# Server de Desenvolvimento PHP da Semapi  #

Repositório padrão do estado do servidor de desenvolvimento, ele deve futuramente ser espelhado com o de produção, quando este for compativel.

## What is this repository for? ##

* Sumário
* Introdução
* Especificações
* Instalação
* Modificações
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Sumário ###

O conteúdo desse documento é facilitar o entendimento por todos os desenvolvedores de como manusear as ferramentas Vagrant e Puppet que serão descritas a seguir. 

Além de apresentar um leve resumo delas, esse repositório tem como objetivo abrir a discussão das escolhas feitas no modo de como será criado o servidor, concentrar os problemas localizados e propostas de desenvolvimento sobre.

### Introdução e Instalação ###

O servidor para o usemol se trat de uma maquina virtual configurada por um script para ser executado pelo Vagrant, que será explicado a seguir, sistema esse que é de configuração de maquinas vituais.

O uso de um sistema já utilizado pelo mercado implica em beneficios, assim como dificuldades que será descritas a seguir

#### Pró ####

* Sistema unificado
* Minimiza a configuração no ambiente de desenvolvimento
* Propagação de modificações e novas ferramentas mais rápida
* Melhoria na verificação de erros de incompatibilidade com o servidor

#### Contra ####

* Nessecidade de conhecimento da ferramenta
* Dependência de tecnologia

### Especificações ###

Segue abaixo os aplicativos que o sistema necessita para executar a máquina virtual

 Sistema                                 | Versão Utilizada | Página de Download                                    | Página de Referencia
---------------------------------------- | ---------------- | ----------------------------------------------------- | -------------------------------------- |
[Git](http://git-scm.com/)               | 2.0.1            | [Download](http://git-scm.com/downloads)              | [Documentacao](http://git-scm.com/doc)                              |
[Vagrant](http://www.vagrantup.com)      | 1.6.3            | [Download](http://www.vagrantup.com/downloads)        | [Documentacao](http://docs.vagrantup.com/v2/)                     |
[Puppet](http://docs.puppetlabs.com)     | 3.6.2            | Já instalado no vagrant*                              | [Documentacao](http://docs.puppetlabs.com/references/3.6.stable/) |
[Vitual Box](https://www.virtualbox.org) | 4.3.12           | [Download](https://www.virtualbox.org/wiki/Downloads) | [Documentacao](https://www.virtualbox.org/wiki/Documentation)     |

#### Submodulos ####

* [Zend Server](https://forge.puppetlabs.com/mwillbanks/zend_server)

### Instalação ###
 
1. Instalar o Git
2. Instalar o Virtual Box
3. Instalar o Vagrant
4. Criar uma conta no [Bitbucket](https://bitbucket.org/)
5. Clonar o esse repositório em uma pasta

```
#!bash
git clone https://rafear@bitbucket.org/rafear/vagrant-usemol.git

```

6. Modificar a configuração
7. dentro da pasta executar
```
#!bash
vagrant up

```

## Modificações ##

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions