#notify { 'Upload do Arquivo zend (necessario para Centos)':}
file { "/etc/yum.repos.d/zend.repo":
    mode   => 755,
    owner  => root,
    group  => root,        
    source => "/vagrant/files/zend.repo",
}

#notify { 'Instalando Zend Server':}
class { "zend_server":
	extra_ext   => true,
}

#notify { 'Configurando server':}
zend_server::extension { "xdebug":
    extension => 'xdebug',
}

zend_server::ini { "date":
    options => { 'date.timezone' => 'America/Sao_Paulo', },
    target  => 'date',
}

#Configuração para error_reporting
zend_server::ini { "error_reporting":
    options => { 'error_reporting' => 'E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_NOTICE', },
    target  => 'error_reporting',
}

#Configuração para xdebug
zend_server::ini { "zend_extension":
    options => { 'zend_extension' => '/usr/local/zend/lib/php_extensions/xdebug.so', },
    target  => 'zend_extension',
}